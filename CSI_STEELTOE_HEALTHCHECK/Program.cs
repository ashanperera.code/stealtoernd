using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Extensions.Logging;
using Steeltoe.Extensions.Configuration;
using Steeltoe.Extensions.Configuration.ConfigServer;
using Steeltoe.Extensions.Configuration.PlaceholderCore;
using Steeltoe.Extensions.Configuration.RandomValue;
using Prometheus.DotNetRuntime;

namespace CSI_STEELTOE_HEALTHCHECK
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DotNetRuntimeStatsBuilder
               .Customize()
               .WithDebuggingMetrics(true)
               .WithContentionStats(SampleEvery.OneEvent)
               .WithJitStats()
               .WithThreadPoolSchedulingStats()
               .WithThreadPoolStats()
               .WithGcStats()
               .StartCollecting();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureAppConfiguration((builder, config) =>
                    {
                        config.SetBasePath(builder.HostingEnvironment.ContentRootPath)
                                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                                .AddEnvironmentVariables();
                    });
                    webBuilder.ConfigureLogging((builderContext, loggingBuilder) =>
                    {
                        loggingBuilder.ClearProviders();
                        loggingBuilder.AddConfiguration(builderContext.Configuration.GetSection("Logging"));
                        loggingBuilder.AddDynamicConsole();
                    });
                    webBuilder.UseStartup<Startup>();
                });
    }
}
