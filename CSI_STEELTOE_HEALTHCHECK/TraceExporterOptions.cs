﻿using Microsoft.Extensions.Configuration;
using System;

namespace CSI_STEELTOE_HEALTHCHECK
{
    public class TraceExporterOptions : ITraceExporterOptions
    {
        internal const int DEFAULT_TIMEOUT = 3;
        internal const string DEFAULT_ENDPOINT = "http://localhost:9090";
        private const string CONFIG_PREFIX = "management:tracing:exporter:prometheus";
        private const string SPRING_APPLICATION_PREFIX = "spring:application";

        public string Endpoint { get; set; }

        public bool ValidateCertificates { get; set; }

        public int TimeoutSeconds { get; set; }

        public string ServiceName { get; set; }

        public bool UseShortTraceIds { get; set; }

        public TraceExporterOptions()
        {
            Endpoint = DEFAULT_ENDPOINT;
        }

        public TraceExporterOptions(string defaultServiceName, IConfiguration configuration) : this()
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            var section = configuration.GetSection(CONFIG_PREFIX);

            if (section != null)
            {
                section.Bind(this);
            }

            if (string.IsNullOrEmpty(ServiceName))
            {
                ServiceName = GetApplicationName(defaultServiceName, configuration);
            }
        }

        internal string GetApplicationName(string defaultName, IConfiguration config)
        {
            var section = config.GetSection(SPRING_APPLICATION_PREFIX);
            if (section != null)
            {
                var name = section["name"];
                if (!string.IsNullOrEmpty(name))
                {
                    return name;
                }
            }

            if (!string.IsNullOrEmpty(defaultName))
            {
                return defaultName;
            }

            return "Unknown";
        }
    }
}
