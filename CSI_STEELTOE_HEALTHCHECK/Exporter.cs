﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using OpenTelemetry.Exporter.Prometheus;
using OpenTelemetry.Stats;
using OpenTelemetry.Context;
using OpenTelemetry.DistributedContext;
using CSI_STEELTOE_HEALTHCHECK.OpenTelemetric;
using OpenTelemetry.Stats.Measures;
using OpenTelemetry.Stats.Aggregations;
using OpenCensus.Tags;

namespace CSI_STEELTOE_HEALTHCHECK
{
    public static class Exporter
    {
        //private static readonly ITagger Tagger = Tags.Tagger;

       
        public static void AddPrometheusExporter(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            services.TryAddSingleton<PrometheusExporter>((p) =>
            {
                return CreateExporter(p, configuration);
            });
        }


        public static PrometheusExporter CreateExporter(IServiceProvider provider, IConfiguration configuration)
        {
            ///UNDER DEVELOPMENT. ONE APPROACH.
            Metric<long> metric = new Metric<long>("sample");

            IStatsRecorder statsRecorder = Stats.StatsRecorder;
            IMeasureLong VideoSize = MeasureLong.Create("my.org/measure/video_size", "size of processed videos", "By");
            IViewName VideoSizeViewName = ViewName.Create("my.org/views/video_size");
            long MiB = 1 << 20;
            IView VideoSizeView = View.Create(
                  VideoSizeViewName,
                  "processed video size over time",
                  VideoSize,
                  Distribution.Create(BucketBoundaries.Create(new List<double>() { 0.0, 16.0 * MiB, 256.0 * MiB })),
                 new List<string>() { "Hello world" });

            var @hostEnvironment = provider.GetRequiredService<IWebHostEnvironment>();
            var @opts = new TraceExporterOptions(@hostEnvironment.ApplicationName, configuration);

            var opencensusOptions = new PrometheusExporterOptions()
            {
                Url = "http://localhost:5739/export-prometheus/"
            };

            var xx = Stats.ViewManager;
            xx.RegisterView(VideoSizeView);
            //xx.AllExportedViews.Add(VideoSizeView);
            return new PrometheusExporter(opencensusOptions, xx);
            //var sss = new StatsComponent();
            

            //if (!@opts.ValidateCertificates)
            //{
            //    var client = new HttpClient(
            //        new HttpClientHandler()
            //        {
            //            ServerCertificateCustomValidationCallback = (mesg, cert, chain, errors) => { return true; }
            //        });
            //    return new PrometheusExporter(opencensusOptions,null);

            //}
            //else
            //{
            //var xxx =  new PrometheusExporter(options:opencensusOptions, sss.ViewManager);
            //}
            //return null;
        }
    }
}
