﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTelemetry.Exporter.Prometheus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;

namespace CSI_STEELTOE_HEALTHCHECK
{
    public static class ExporterBuilder
    {
        public static void UsePrometheusExporter(this IApplicationBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            var service = builder.ApplicationServices.GetRequiredService<PrometheusExporter>();
            var lifetime = builder.ApplicationServices.GetRequiredService<IApplicationLifetime>();

            lifetime.ApplicationStopping.Register(() => service.Stop());
            service.Start();
        }
    }
}
