﻿
namespace CSI_STEELTOE_HEALTHCHECK
{
    public interface ITraceExporterOptions
    {
        string Endpoint { get; }
        bool ValidateCertificates { get; }
        int TimeoutSeconds { get; }
        string ServiceName { get; }
        bool UseShortTraceIds { get; }
    }
}
