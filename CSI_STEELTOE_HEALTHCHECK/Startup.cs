using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.CloudFoundry.Connector.MongoDb;
using Steeltoe.CloudFoundry.Connector.MySql;
using Steeltoe.CloudFoundry.Connector.OAuth;
using Steeltoe.CloudFoundry.Connector.PostgreSql;
using Steeltoe.CloudFoundry.Connector.RabbitMQ;
using Steeltoe.CloudFoundry.Connector.Redis;
using Steeltoe.CloudFoundry.Connector.SqlServer;
using Steeltoe.Discovery.Client;
using Steeltoe.Extensions.Configuration.CloudFoundry;
using Steeltoe.Management.Endpoint.Env;
using Steeltoe.Management.Endpoint.Health;
using Steeltoe.Management.Endpoint.HeapDump;
using Steeltoe.Management.Endpoint.Hypermedia;
using Steeltoe.Management.Endpoint.Info;
using Steeltoe.Management.Endpoint.Loggers;
using Steeltoe.Management.Endpoint.Mappings;
using Steeltoe.Management.Endpoint.Metrics;
using Steeltoe.Management.Endpoint.Refresh;
using Steeltoe.Management.Endpoint.ThreadDump;
using Steeltoe.Management.Endpoint.Trace;
using Steeltoe.Management.Exporter.Metrics;
using Prometheus.Client.HttpRequestDurations;
using Steeltoe.Management.Exporter.Tracing;
using OpenTelemetry.Trace.Configuration;
using OpenTelemetry.Exporter.Prometheus;
using OpenTelemetry.Stats;
using Prometheus;
using CSI_STEELTOE_HEALTHCHECK.Extensions;

namespace CSI_STEELTOE_HEALTHCHECK
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddHypermediaActuator(Configuration);
            services.AddHealthActuator(Configuration);
            services.AddInfoActuator(Configuration);
            services.AddLoggersActuator(Configuration);
            services.AddTraceActuator(Configuration);
            services.AddThreadDumpActuator(Configuration);
            services.AddHeapDumpActuator(Configuration);
            services.AddEnvActuator(Configuration);
            services.AddRefreshActuator(Configuration);
            services.AddMappingsActuator(Configuration);
            services.AddMetricsActuator(Configuration);
            services.AddSqlServerConnection(Configuration);
            services.AddRabbitMQConnection(Configuration);
            services.AddMySqlConnection(Configuration);
            services.AddPostgresConnection(Configuration);

            services.AddPrometheusExporter(Configuration);
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseHttpMetrics();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UsePrometheusRequestDurations(q =>
            {
                q.IncludeMethod = true;
                q.IncludePath = true;
                q.IncludeStatusCode = true;
            });
            app.UseMetricServer();
            app.UseExtensionMiddleWare();

            app.UseRouting();
            app.UseAuthorization();
            app.UseHypermediaActuator();
            app.UseHealthActuator();
            app.UseInfoActuator();
            app.UseLoggersActuator();
            app.UseTraceActuator();
            app.UseThreadDumpActuator();
            app.UseHeapDumpActuator();
            app.UseEnvActuator();
            app.UseRefreshActuator();
            app.UseMappingsActuator();
            app.UseMetricsActuator();

            app.UsePrometheusExporter();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
