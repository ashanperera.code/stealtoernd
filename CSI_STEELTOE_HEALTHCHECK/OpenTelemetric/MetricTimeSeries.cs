﻿using OpenTelemetry.Metrics;
using System.Collections.Generic;

namespace CSI_STEELTOE_HEALTHCHECK.OpenTelemetric
{
    public class MetricTimeSeries<T>
    {
        public LabelSet LabelSet;
        public List<T> Points = new List<T>();

        public MetricTimeSeries(LabelSet labelSet)
        {
            this.LabelSet = labelSet;
        }

        public void Add(T value)
        {
            this.Points.Add(value);
        }
    }
}
