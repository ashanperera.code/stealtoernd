﻿using OpenTelemetry.Metrics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace CSI_STEELTOE_HEALTHCHECK.OpenTelemetric
{
    public class Metric<T>
    {
        public Metric(string name)
        {
            this.MetricName = name;
            this.MetricDescription = "Description:" + name;
        }

        public string MetricName { get; private set; }

        public string MetricDescription { get; private set; }

        public IDictionary<LabelSet, MetricTimeSeries<T>> TimeSeries { get; } = new ConcurrentDictionary<LabelSet, MetricTimeSeries<T>>();

        public MetricTimeSeries<T> GetOrCreateMetricTimeSeries(LabelSet labelSet)
        {
            if (this.TimeSeries.ContainsKey(labelSet))
            {
                return this.TimeSeries[labelSet];
            }
            else
            {
                return this.CreateMetricTimeSeries(labelSet);
            }
        }

        private MetricTimeSeries<T> CreateMetricTimeSeries(LabelSet labelSet)
        {
            var newSeries = new MetricTimeSeries<T>(labelSet);
            this.TimeSeries.Add(labelSet, newSeries);
            return newSeries;
        }
    }
}
