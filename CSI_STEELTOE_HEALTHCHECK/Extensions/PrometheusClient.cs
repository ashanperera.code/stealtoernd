﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Prometheus;
using System;
using System.Threading.Tasks;

namespace CSI_STEELTOE_HEALTHCHECK.Extensions
{
    public class PrometheusClient
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        public PrometheusClient(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<PrometheusClient>();
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var path = httpContext.Request.Path.Value;
            var method = httpContext.Request.Method;

            var counter = Metrics.CreateCounter("total_no_requests", "HTTP Requests Total", new CounterConfiguration
            {
                LabelNames = new[] { "path", "method", "status" }
            });

            var statusCode = 200;

            try
            {
                await _next.Invoke(httpContext);
            }
            catch (Exception)
            {
                statusCode = 500;
                counter.Labels(path, method, statusCode.ToString()).Inc();

                throw;
            }

            if (path != "/metrics")
            {
                statusCode = httpContext.Response.StatusCode;
                counter.Labels(path, method, statusCode.ToString()).Inc();
            }
        }
    }
    public static class RequestMiddlewareExtensions
    {
        public static IApplicationBuilder UseExtensionMiddleWare(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<PrometheusClient>();
        }
    }
}
